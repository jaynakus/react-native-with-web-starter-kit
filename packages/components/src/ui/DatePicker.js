import React from 'react';
import {AppInput} from "./Editable";
import moment from 'moment';
import Modal from "react-native-modal";
import {Calendar} from "react-native-calendars";
import {TouchableOpacity} from 'react-native';

export default class DatePicker extends React.Component {
  input;

  constructor(props, context) {
    super(props, context);
    this.state = {show: false};
  }

  _to = (value) => {
    return moment(value).toDate();
  };

  _from = (value) => {
    return moment(value).format('DD/MM/YYYY')
  };

  _toggleModal = () => {
    this.setState({show: !this.state.show});
    if (this.input && this.input.blur)
      this.input.blur();
  };
  _onSelect = (day) => {
    this._toggleModal();
    let {model, name} = this.props;
    console.log(day);
    model.setValue(name, new Date(day.timestamp));
  };

  render() {
    let {model, name, ...rest} = this.props;
    val = '';
    if (model && name)
      val = model[name];
    return [
      <TouchableOpacity onPress={this._toggleModal} style={{width: '100%'}}>
        <AppInput model={model} name={name} ref={(r) => this.input = r} converter={{to: this._to, from: this._from}}
                  rightIcon={{name: 'ios-calendar', type: 'ionicon'}} editable={false}
                  onTouchStart={this._toggleModal}
                  {...rest}/>
      </TouchableOpacity>,
      <Modal isVisible={this.state.show} onBackdropPress={this._toggleModal}>
        <Calendar current={val} onDayPress={this._onSelect}/>
      </Modal>
    ]
  }
}
