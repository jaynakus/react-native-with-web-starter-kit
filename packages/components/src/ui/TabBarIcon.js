import React from 'react';
import {Icon} from "react-native-elements";
import Colors from "./Colors";

export default class TabBarIcon extends React.Component {
  render() {
    return (
      <Icon type='ionicon' size={26} name={this.props.name} style={{marginBottom: -3}}
            color={this.props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}/>
    );
  }
}
