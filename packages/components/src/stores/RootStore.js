import {types, onSnapshot, getSnapshot} from 'mobx-state-tree';
import {CompanyStore} from './CompanyStore';
import {UserStore} from './UserStore';
import {NavigationStore} from './NavigationStore';
import {AsyncStorage} from 'react-native';

export const RootStore = types
  .model('RootStore', {
    identifier: types.optional(types.identifier, 'RootStore'),
    userStore: types.optional(UserStore, () => UserStore.create({users: {}})),
    companyStore: types.optional(CompanyStore, () => CompanyStore.create({repos: []})),
    navigationStore: types.optional(NavigationStore, () =>
      NavigationStore.create({companyDetailScreenParams: {}, userScreenParams: {}})
    ),
  })
  .actions(self => ({
    async save() {
      try {
        const transformedSnapshot = getSnapshot(self);
        const json = JSON.stringify(transformedSnapshot);

        await AsyncStorage.setItem('appStatePersistenceKey', json);
      } catch (err) {
        console.warn('unexpected error ' + err);
      }
    },
  }));
