import {flow, getRoot, onSnapshot, types} from 'mobx-state-tree';
import {Company} from '../models/Company';

export const CompanyStore = types
  .model('CompanyStore', {
    identifier: types.optional(types.identifier, 'CompanyStore'),
    repos: types.array(Company),
    username: types.optional(types.string, 'guest'),
  })
  .views(self => ({
    getRandomRepo() {
      const index = Math.floor(Math.random() * self.repos.length);
      return self.repos[index];
    },
  }))
  .actions(self => {
    const setRepos = reposArray => {
      const {userStore} = getRoot(self);

      self.repos = reposArray.map(it => {
        const owner = userStore.createOrGetUser(it.owner);
        return Repo.create({...it, id: `repo_${it.id}`, owner});
      });
    };

    return {
      fetchRepos: flow(function* () {
        if (!self.username) {
          return;
        }
        const reposJson = yield fetch(`https://api.github.com/users/${self.username}/repos`).then(
          resp => resp.json()
        );
        setRepos(reposJson);
        return reposJson;
      }),
      setUsername(value) {
        self.username = value;
      },
      afterCreate() {
        onSnapshot(self, () => {
          const rootStore = getRoot(self);
          rootStore.save();
        });
      },
    };
  });
