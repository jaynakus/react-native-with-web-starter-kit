import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMainTabNavigator} from './MainTabNavigation';
import {AuthLoadingScreen, SignInScreen} from "../screens/SignInScreen";

const AuthStack = createStackNavigator({SignIn: SignInScreen});

export const createAppNavigator = rootStore => {
  const App = createMainTabNavigator(rootStore);
  return createAppContainer(
    createSwitchNavigator(
      {
        AuthLoading: AuthLoadingScreen,
        App,
        Auth: AuthStack,
      },
      {
        initialRouteName: 'AuthLoading',
      }
    ))
};
