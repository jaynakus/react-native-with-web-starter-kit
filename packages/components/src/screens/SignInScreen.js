import React from 'react';
import {Button, AsyncStorage, ActivityIndicator} from 'react-native';
import {ScreenWrapper} from "./Components";

export class SignInScreen extends React.Component {
  static navigationOptions = {
    title: 'Please sign in',
  };

  render() {
    return (
      <ScreenWrapper>
        <Button title="Sign in!" onPress={this._signInAsync}/>
      </ScreenWrapper>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };
}

export class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <ScreenWrapper>
        <ActivityIndicator/>
      </ScreenWrapper>
    );
  }
}
