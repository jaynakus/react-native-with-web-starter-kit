import styled from 'styled-components';
import {View} from 'react-native';

export const ScreenWrapper = styled(View)`
  flex: 1;
  background-color: #F5FCFF;
  justify-content: center;
  align-items: center;
`;
