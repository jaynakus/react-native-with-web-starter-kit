import React from "react";
import {Text, Platform} from "react-native";
import {ScreenWrapper} from "./Components";
import styled from 'styled-components';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
  web: 'Press Cmd+R to reload'
});
const WelcomeText = styled(Text)`
  font-size: 20;
  text-align: center;
  margin: 10px;
`;
const InstructionText = styled(Text)`
  text-align: center;
  color: #333333;
  margin-bottom: 5;
`;

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Docs',
  };

  render() {
    return (
      <ScreenWrapper>
        <WelcomeText>Welcome to React Native monorepo!</WelcomeText>
        <InstructionText>To get started, edit App.js</InstructionText>
        <InstructionText>{instructions}</InstructionText>
      </ScreenWrapper>
    );
  }
}



