import React, {Component} from 'react';
import {RootStore} from "./stores/RootStore";
import NavigationService from "./navigation/NavigationService";
import {Provider} from "mobx-react";
import {createAppNavigator} from "./navigation/AppNavigator";
import {AsyncStorage, Platform} from 'react-native';
import RNRestart from 'react-native-restart';

const navigationPersistenceKey = __DEV__ ? 'NavigationStateDEV' : null;

const persistNavigationState = async (navState) => {
  try {
    await AsyncStorage.setItem(navigationPersistenceKey, JSON.stringify(navState))
  } catch (err) {
    // handle the error according to your needs
  }
};
const loadNavigationState = async () => {
  const jsonString = await AsyncStorage.getItem(navigationPersistenceKey);
  return JSON.parse(jsonString)
};

export default class App extends Component {

  componentDidCatch(error, errorInfo) {
    // To avoid navigation persistence error when some routes get removed,
    // delete old persistence data and restart the app
    if (error.message.indexOf('key') >= 0) {
      AsyncStorage.removeItem(navigationPersistenceKey);
      if (Platform.OS !== 'web')
        RNRestart.Restart();
      else
        window.location.reload();
    }
  }

  constructor(props) {
    super(props);
    this.rootStore = RootStore.create({});
    NavigationService.setNavigationStore(this.rootStore.navigationStore);
  }

  render() {
    const AppNavigator = createAppNavigator(this.rootStore);
    return (
      <Provider rootStore={this.rootStore}>
        <AppNavigator
          persistNavigationState={persistNavigationState} loadNavigationState={loadNavigationState}
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}
