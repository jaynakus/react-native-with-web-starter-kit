/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from '@docs/components/src/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
