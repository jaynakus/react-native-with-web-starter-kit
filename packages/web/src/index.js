import {AppRegistry} from 'react-native'

import App from '@docs/components/src/App'

AppRegistry.registerComponent('docs', () => App);
AppRegistry.runApplication('docs', {  rootTag: document.getElementById('root'),});
