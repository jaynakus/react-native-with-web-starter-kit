This is a starter kit I'm going to use in my further private projects. If someone finds it helpful cheers.

React and react-native 100% code sharing starter kit based on https://dev.to/brunolemos/tutorial-100-code-sharing-between-ios-android--web-using-react-native-web-andmonorepo-4pej

Added

1) react-navigation: with persistence with mobx-state-tree
2) styled-components: to ease some styling
